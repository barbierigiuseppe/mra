package tdd.training.mra;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

public class MarsRoverTest {

	@Test
	public void testPlanetShouldHaveAnObstacle() throws MarsRoverException {
		
		List<String>planetObstacles = new ArrayList<String>();
		
		MarsRover rover = new MarsRover(10, 12, planetObstacles);
		
		planetObstacles.add("(5,5)");
		
		assertTrue(rover.planetContainsObstacleAt(5, 5));
		
	}

	@Test
	public void testPlanetShouldBe10x10WithTwoObstacles() throws MarsRoverException {
		
		List<String>planetObstacles = new ArrayList<String>();
		
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		planetObstacles.add("(0,1)");
		planetObstacles.add("(2,2)");
		
		assertTrue(rover.planetContainsObstacleAt(0, 1));
		assertTrue(rover.planetContainsObstacleAt(2, 2));
		
	}
	
	@Test
	public void testRoverShouldBeAtDefaultPosition() throws MarsRoverException{
		List<String>planetObstacles = new ArrayList<String>();
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		String status = "(0,0,N)";
		String commandString = "";
		String returnString = status;
		
		assertEquals(returnString,rover.executeCommand(commandString));
	}
	
	@Test
	public void testRoverShouldTurnRight() throws MarsRoverException{
		List<String>planetObstacles = new ArrayList<String>();
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		String status = "(0,0,E)";
		String commandString = "r";
		String returnString = status;
		
		assertEquals(returnString,rover.executeCommand(commandString));
	}
	
	@Test
	public void testRoverShouldTurnLeft() throws MarsRoverException{
		List<String>planetObstacles = new ArrayList<String>();
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		String status = "(0,0,W)";
		String commandString = "l";
		String returnString = status;
		
		assertEquals(returnString,rover.executeCommand(commandString));
	}
	
	@Test
	public void testRoverShouldMoveForwardFacingNorth() throws MarsRoverException{
		List<String>planetObstacles = new ArrayList<String>();
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		String commandString = "f";
		String status = "(0,1,N)";
		String returnString = status;
		
		assertEquals(returnString,rover.executeCommand(commandString));
		
		
	}
	
	@Test
	public void testRoverShouldMoveBackwardsFacingNorth() throws MarsRoverException{
		List<String>planetObstacles = new ArrayList<String>();
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		String commandString = "b";
		rover.executeCommand("f");
		rover.executeCommand("f");
		String returnString = ("(0,1,N)");
		
		assertEquals(returnString,rover.executeCommand(commandString));
		
		
	}
	
	@Test
	public void testRoverShouldMoveCominingMultipleCommands() throws MarsRoverException{
		List<String>planetObstacles = new ArrayList<String>();
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		String commandString = "ffrff";
		rover.executeCommand("f");
		rover.executeCommand("f");
		rover.executeCommand("r");
		rover.executeCommand("f");
		rover.executeCommand("f");
		String returnString = ("(2,2,E)");
		
		assertEquals(returnString,rover.executeCommand(commandString));
		
		
	}
	
	@Test
	public void testPlanetIsASphere() throws MarsRoverException{
		List<String>planetObstacles = new ArrayList<String>();
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		String commandString = "b";
		String returnString = ("(0,9,N)");
		
		assertEquals(returnString,rover.executeCommand(commandString));
		
		
	}
	
}
