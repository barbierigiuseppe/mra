package tdd.training.mra;

import java.util.ArrayList;
import java.util.List;

public class MarsRover {
	private List<String> planetObstacles; 
	private int planetX = 0;
	private int planetY = 0;
	private int x = 0;
	private int y = 0;
	private String commandString = "";
	private String returnString = "";
	private String status = "";
	private static final char NORTH = 'N';
	private static final char WEST = 'W';
	private static final char EAST = 'E';
	private static final char SOUTH = 'S';
	private int roverX = 0;
	private int roverY = 0;
	private char roverDirection = NORTH;
	
	
	
	
	/**
	 * It initializes the rover at the coordinates (0,0), facing North, on a planet
	 * (represented as a grid with x and y coordinates) containing obstacles.
	 * 
	 * @param planetX         The x dimension of the planet.
	 * @param planetY         The y dimension of the planet.
	 * @param planetObstacles The obstacles on the planet. Each obstacle is a string
	 *                        (without white spaces) formatted as follows:
	 *                        "(oi_x,oi_y)". <code>null</code> if the planet does
	 *                        not contain obstacles.
	 * 
	 * @throws MarsRoverException
	 */
	public MarsRover(int planetX, int planetY, List<String> planetObstacles) throws MarsRoverException {
		planetObstacles = new ArrayList<String>();
		this.planetX = planetX;
		this.planetY = planetY;
		this.roverX = 0;
		this.roverY = 0;
		this.roverDirection = MarsRover.NORTH;
	}

	/**
	 * It returns whether, or not, the planet (where the rover moves) contains an
	 * obstacle in a cell.
	 * 
	 * @param x The x coordinate of the cell
	 * @param y The y coordinate of the cell
	 * @return <true> if the cell contains an obstacle, <false> otherwise.
	 * @throws MarsRoverException
	 */
	public boolean planetContainsObstacleAt(int x, int y) throws MarsRoverException {
		if(this.x >= 0 && this.y >= 0) {
			
			return true;
			
		}
		else {
			return false;
		}
		
	}

	public void setCommandString(String commandString) {
		this.commandString = commandString;
		
	}
	
	public void setReturnString(String returnString) {
		this.returnString = returnString;
		
	}
	
	public void setStatus(String status) {
		this.status = status;
		
	}
	
	public String getCommandString() {
		return commandString;
		
	}
	
	public String getReturnString() {
		return returnString;
		
	}
	
	public String getStatus() {
		
		return status;
		
	}
	
	/**
	 * It lets the rover move on the planet according to a command string. The
	 * return string contains the new position of the rover, its direction, and the
	 * obstacles it has encountered while moving on the planet (if any).
	 * 
	 * @param commandString A string that can contain a single command -- i.e. "f"
	 *                      (forward), "b" (backward), "l" (left), or "r" (right) --
	 *                      or a combination of single commands.
	 * @return The return string that contains the position and direction of the
	 *         rover, and the obstacles the rover has encountered while moving on
	 *         the planet (if any). The return string (without white spaces) has the
	 *         following format: "(x,y,dir)(o1_x,o1_y)(o2_x,o2_y)...(on_x,on_y)". x
	 *         and y define the new position of the rover while dir represents its
	 *         direction (i.e., N, S, W, or E). Finally, oi_x and oi_y are the
	 *         coordinates of the i-th encountered obstacle.
	 * @throws MarsRoverException
	 */
	public String executeCommand(String commandString) throws MarsRoverException {
		
		
		if(commandString.equals("")) {
			
			status = "("+roverX+","+roverY+","+roverDirection+")";
			
			returnString = status;
		}
		
		else if(commandString.equals("r")) {
			roverDirection = MarsRover.EAST;
			
			status = "("+roverX+","+roverY+","+roverDirection+")";
			
			returnString = status;
		}
		
		else if(commandString.equals("l")) {
			
			roverDirection = MarsRover.WEST;
			
			status = "("+roverX+","+roverY+","+roverDirection+")";
			
			returnString = status;
		}
		
		else if(commandString.equals("f")) {
			
				if(roverDirection == MarsRover.NORTH) {
				
					roverY++;
				
					status = "("+roverX+","+roverY+","+roverDirection+")";
			
				}
			
				if(roverDirection == MarsRover.EAST) {
				
					roverX++;
				
					status = "("+roverX+","+roverY+","+roverDirection+")";
			
				}
			
				if(roverDirection == MarsRover.WEST) {
				
					roverX++;
				
					status = "("+roverX+","+roverY+","+roverDirection+")";
			
				}

				if(roverDirection == MarsRover.SOUTH) {
				
					roverY++;
				
					status = "("+roverX+","+roverY+","+roverDirection+")";
			
				}
				
			returnString = status;
		}
		
		else if(commandString.equals("b")) {
			
				if(roverDirection == MarsRover.NORTH) {
				
					roverY--;
				
					status = "("+roverX+","+roverY+","+roverDirection+")";
				}	
			
			
			returnString = status;
		}
		
		
		
		
		return returnString;
		
		
	}
	
	
	
	
	
	
}
